const express = require('express');
const app = express();

app.use(express.json());

const courses = [
    { id: 1, name: "course 1"},
    { id: 2, name: "course 2"},
    { id: 3, name: "course 3"},
];

app.get('/', (req, res) => {
    res.send('<h1><center> Hello World </center></h1>');
});

app.get('/api/courses', (req, res)=>{
    res.send(courses);
});

app.get('/api/courses/:id', (req, res) =>{
    const course = courses.find(c => c.id === parseInt(req.params.id));
    res.send(course);
});

app.post('/api/courses', (req, res) => {
    const course = {
        id : courses.length + 1,
        name : req.body.name,
    };
    courses.push(course);
    res.send(course);
});

const PORT = process.env.PORT || 3000;
app.listen(PORT);